package it.primaryteam.webapp.whisper.feedback;

import it.primaryteam.webapp.whisper.core.BaseDomain;
import it.primaryteam.webapp.whisper.job.Job;

public class Feedback extends BaseDomain <Long> {

	private static final long serialVersionUID = 1L; 

	private String q1;
	private String q2;
	private String q2bis;
	private String q3;
	private String q4;
	private String q4bis;
	private String q5;  //case
	private String q6;
	private String q7;
	private Job job = new Job();
	public String getQ1() {
		return q1;
	}
	public void setQ1(String q1) {
		this.q1 = q1;
	}
	public String getQ2() {
		return q2;
	}
	public void setQ2(String q2) {
		this.q2 = q2;
	}
	public String getQ2bis() {
		return q2bis;
	}
	public void setQ2bis(String q2bis) {
		this.q2bis = q2bis;
	}
	public String getQ3() {
		return q3;
	}
	public void setQ3(String q3) {
		this.q3 = q3;
	}
	public String getQ4() {
		return q4;
	}
	public void setQ4(String q4) {
		this.q4 = q4;
	}
	public String getQ4bis() {
		return q4bis;
	}
	public void setQ4bis(String q4bis) {
		this.q4bis = q4bis;
	}
	public String getQ5() {
		return q5;
	}
	public void setQ5(String q5) {
		this.q5 = q5;
	}
	public String getQ6() {
		return q6;
	}
	public void setQ6(String q6) {
		this.q6 = q6;
	}
	public String getQ7() {
		return q7;
	}
	public void setQ7(String q7) {
		this.q7 = q7;
	}
	public Job getJob() {
		return job;
	}
	public void setJob(Job job) {
		this.job = job;
	}
	@Override
	public String toString() {
		return "Feedback [q1=" + q1 + ", q2=" + q2 + ", q2bis=" + q2bis + ", q3=" + q3 + ", q4=" + q4 + ", q4bis="
				+ q4bis + ", q5=" + q5 + ", q6=" + q6 + ", q7=" + q7 + ", job=" + job + "]";
	}

}