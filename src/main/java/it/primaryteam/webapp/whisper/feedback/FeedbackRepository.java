package it.primaryteam.webapp.whisper.feedback;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import javax.enterprise.context.Dependent;
import it.primaryteam.webapp.whisper.core.BaseRepository;
import it.primaryteam.webapp.whisper.feedback.Feedback;
import it.primaryteam.webapp.whisper.job.Job;

@Dependent
public class FeedbackRepository extends BaseRepository <Long, Feedback> {

	public void add(Feedback entity) {

		String sql = String.format("INSERT INTO Feedbacks (q1, q2, q2bis, q3, q4, q4bis, q5, q6, q7, idjob, visibility) VALUES "
				+ "('%s', '%s','%s','%s','%s','%s','%s', '%s','%s', %d, true)", 
				entity.getQ1(), entity.getQ2(), entity.getQ2bis(),entity.getQ3(),entity.getQ4(),
				entity.getQ4bis(),entity.getQ5(),entity.getQ6(),entity.getQ7(), entity.getJob().getId());

		req(sql);
	}

	@Override
	public Feedback findById(Long id) {

		Job job = new Job();
		Feedback  entity = new Feedback();

		String sql = String.format("Select * FROM Feedbacks f WHERE f.visibility=true AND f.idjob = %d", id);

		List<Map<String, Object>> results = select(sql);

		for(Map<String, Object> result : results) {

			entity.setId((Long)result.get("id"));
			entity.setQ1((String)result.get("q1"));
			entity.setQ2((String)result.get("q2"));
			entity.setQ2bis((String)result.get("q2bis"));
			entity.setQ3((String)result.get("q3"));
			entity.setQ4((String)result.get("q4"));
			entity.setQ4bis((String)result.get("q4bis"));
			entity.setQ5((String)result.get("q5"));
			entity.setQ6((String)result.get("q6"));
			entity.setQ7((String)result.get("q7"));

			job.setId((Long)result.get("idjob"));
			entity.setJob(job);

			entity.setVisibility(true);
		}
		return entity;
	}

	@Override
	public void update(Feedback entity) {

		String sql = String.format("UPDATE Feedbacks f  SET q1 = '%s', q2 = '%s', q2bis = '%s', q3 = '%s', q4 = '%s', "
				+ "q4bis = '%s', q5 = '%s', q6 = '%s', q7 = '%s', idjob= %d, visibility = %b"  
				+ " WHERE f.id = %d",entity.getQ1(), entity.getQ2(), entity.getQ2bis(),entity.getQ3(),entity.getQ4(),
				entity.getQ4bis(),entity.getQ5(),entity.getQ6(),entity.getQ7(), entity.getJob().getId(), entity.isVisibility(), entity.getId());

		req(sql);
	}

	@Override
	public void delete(Feedback entity) {

		String sql = String.format("UPDATE Feedbacks f SET f.visibility = false WHERE id = %d", entity.getId());

		req(sql);
	}

	@Override
	public Collection<Feedback> findAll() {

		List<Feedback> entitiesList = new ArrayList<Feedback>();

		String sql = "SELECT * FROM Feedbacks as f, Jobs j WHERE f.idjob = j.id AND f.visibility = true ";


		List<Map<String, Object>> results = select(sql);

		for(Map<String, Object> result : results) {

			Feedback entity = new Feedback();
			Job job = new Job();

			entity.setId((Long)result.get("id"));
			entity.setQ1((String)result.get("q1"));
			entity.setQ2((String)result.get("q2"));
			entity.setQ2bis((String)result.get("q2bis"));
			entity.setQ3((String)result.get("q3"));
			entity.setQ4((String)result.get("q4"));
			entity.setQ4bis((String)result.get("q4bis"));
			entity.setQ5((String)result.get("q5"));
			entity.setQ6((String)result.get("q6"));
			entity.setQ7((String)result.get("q7"));

			job.setId((Long)result.get("idjob"));
			entity.setJob(job);

			job.setCode((String)result.get("code"));
			job.setDescription((String)result.get("description"));

			entity.setVisibility(true);

			entitiesList.add(entity);
		}

		return entitiesList;
	}

	public List<Feedback> search(String column, String value) {



		List<Feedback> entitiesList = new ArrayList<Feedback>();

		String sql = "SELECT * FROM Feedbacks as f WHERE f.visibility=true AND " + column + " = '" + value + "'";

		List<Map<String, Object>> results = select(sql);

		for(Map<String, Object> result : results) {

			Feedback entity = new Feedback();

			Job job = new Job();

			entity.setId((Long)result.get("id"));
			entity.setQ1((String)result.get("q1"));
			entity.setQ2((String)result.get("q2"));
			entity.setQ2bis((String)result.get("q2bis"));
			entity.setQ3((String)result.get("q3"));
			entity.setQ4((String)result.get("q4"));
			entity.setQ4bis((String)result.get("q4bis"));
			entity.setQ5((String)result.get("q5"));
			entity.setQ6((String)result.get("q6"));
			entity.setQ7((String)result.get("q7"));

			job.setId((Long)result.get("idjob"));
			entity.setJob(job);

			entity.setVisibility(true);

			entitiesList.add(entity);
		}
		return entitiesList;
	}

	@Override
	public Collection<Feedback> findAllById(Long id) {
		return null;
	}
}