package it.primaryteam.webapp.whisper.qualification;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.enterprise.context.Dependent;
import it.primaryteam.webapp.whisper.core.BaseRepository;
import it.primaryteam.webapp.whisper.customer.Customer;

@Dependent
public class QualificationRepository extends BaseRepository <Long, Qualification> {

	public void add(Qualification entity) {

		String sql = String.format(Locale.US," INSERT INTO Qualifications (fascia, qualifica, cv, docenza, cd, sw,"
				+ " elmat, ric, prog, eldid, tutor, cons, altro, scad, grad, tot, comp1, comp2, data, anno, idcustomer, visibility) VALUES "
				+ "('%s','%s','%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s','%s',%d, %d,  %d, '%s', '%s', '%s', '%s', %d, true)", 
				entity.getFascia(), entity.getQualifica(), entity.getCv(), entity.getDocenza(), entity.getCd(),
				entity.getSw(),entity.getElmat(), entity.getRic(), entity.getProg(), entity.getEldid(), entity.getTutor(),
				entity.getCons(), entity.getAltro(), entity.getScad(), entity.getGrad(), entity.getScad() + entity.getGrad(), entity.getComp1(),
				entity.getComp2(), new Timestamp(entity.getData().getTime()),new Timestamp(entity.getAnno().getTime()), 
				entity.getCustomer().getId());

		req(sql);
	}

	@Override
	public Qualification findById(Long id) {

		Customer customer = new Customer();

		Qualification entity= new Qualification();

		String sql = String.format("Select * FROM Qualifications q WHERE q.idcustomer = %d", id);

		List<Map<String, Object>> results = select(sql);

		for(Map<String, Object> result : results) {

			entity.setId((Long)result.get("id"));
			entity.setFascia((String)result.get("fascia"));
			entity.setQualifica((String)result.get("qualifica"));
			entity.setCv((String)result.get("cv"));
			entity.setDocenza((String)result.get("docenza"));
			entity.setCd((String)result.get("cd"));
			entity.setSw((String)result.get("sw"));
			entity.setElmat((String)result.get("elmat"));
			entity.setRic((String)result.get("ric"));
			entity.setProg((String)result.get("prog"));
			entity.setEldid((String)result.get("eldid"));
			entity.setTutor((String)result.get("tutor"));
			entity.setCons((String)result.get("cons"));
			entity.setAltro((String)result.get("altro"));
			entity.setScad((Integer)result.get("scad"));
			entity.setGrad((Integer)result.get("grad"));	
			entity.setTot((Integer)result.get("tot"));
			entity.setComp1((String)result.get("comp1"));
			entity.setComp2((String)result.get("comp2"));
			entity.setData(new Date(((Timestamp)result.get("data")).getTime()));
			entity.setAnno(new Date(((Timestamp)result.get("anno")).getTime()));

			customer.setId((Long)result.get("idcustomer"));
			entity.setCustomer(customer);

			entity.setVisibility(true);
		}
		return entity;
	}

	@Override
	public void update(Qualification entity) {

		String sql = String.format("UPDATE Qualifications q  SET fascia = '%s', qualifica = '%s', cv = '%s', docenza = '%s', cd = '%s', "
				+ "sw = '%s', elmat = '%s', ric = '%s', prog = '%s', eldid = '%s', tutor = '%s', cons = '%s', altro = '%s', scad = %d, "
				+ "grad = %d, tot = %d, comp1 = '%s', comp2 = '%s', data = '%s', anno = '%s', idcustomer = %d, visibility = true "
				+ "WHERE q.id = %d", 
				entity.getFascia(), entity.getQualifica(), entity.getCv(), entity.getDocenza(), entity.getCd(),
				entity.getSw(),entity.getElmat(), entity.getRic(), entity.getProg(), entity.getEldid(), entity.getTutor(),
				entity.getCons(), entity.getAltro(), entity.getScad(), entity.getGrad(), entity.getScad() + entity.getGrad(), entity.getComp1(),
				entity.getComp2(), new Timestamp(entity.getData().getTime()),new Timestamp(entity.getAnno().getTime()), 
				entity.getCustomer().getId(), entity.getId());

		req(sql);
	}

	@Override
	public void delete(Qualification entity) {

	}

	@Override
	public Collection<Qualification> findAll() {

		List<Qualification> entitiesList = new ArrayList<Qualification>();

		String sql = "SELECT * FROM Qualifications as q, Customers as c WHERE c.id=q.idcustomer AND q.visibility=true";

		List<Map<String, Object>> results = select(sql);

		for(Map<String, Object> result : results) {

			Qualification entity= new Qualification();
			Customer customer = new Customer();

			entity.setId((Long)result.get("id"));
			entity.setFascia((String)result.get("fascia"));
			entity.setQualifica((String)result.get("qualifica"));
			entity.setCv((String)result.get("cv"));
			entity.setDocenza((String)result.get("docenza"));
			entity.setCd((String)result.get("cd"));
			entity.setSw((String)result.get("sw"));
			entity.setElmat((String)result.get("elmat"));
			entity.setRic((String)result.get("ric"));
			entity.setProg((String)result.get("prog"));
			entity.setEldid((String)result.get("eldid"));
			entity.setTutor((String)result.get("tutor"));
			entity.setCons((String)result.get("cons"));
			entity.setAltro((String)result.get("altro"));
			entity.setScad((Integer)result.get("scad"));
			entity.setGrad((Integer)result.get("grad"));	
			entity.setTot((Integer)result.get("tot"));
			entity.setComp1((String)result.get("comp1"));
			entity.setComp2((String)result.get("comp2"));
			entity.setData(new Date(((Timestamp)result.get("data")).getTime()));
			entity.setAnno(new Date(((Timestamp)result.get("anno")).getTime()));

			customer.setId((Long)result.get("idcustomer"));
			customer.setCompany((String)result.get("company"));
			customer.setOwner((String)result.get("owner"));
			entity.setCustomer(customer);

			entity.setVisibility(true);

			entitiesList.add(entity);
		}

		return entitiesList;
	}

	@Override
	public List<Qualification> search(String column, String value) {

		return null;
	}

	@Override
	public Collection<Qualification> findAllById(Long id) {
		return null;
	}

}