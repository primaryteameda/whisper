package it.primaryteam.webapp.whisper.qualification;

import java.util.Date;
import javax.validation.constraints.NotNull;
import it.primaryteam.webapp.whisper.core.BaseDomain;
import it.primaryteam.webapp.whisper.customer.Customer;

public class Qualification  extends BaseDomain <Long>{

	private static final long serialVersionUID = 1L; 

	Customer customer = new Customer();

	private String fascia;
	private String qualifica;
	private String cv;
	private String docenza;
	private String cd;
	private String sw;
	private String elmat;
	private String ric;
	private String prog;
	private String eldid;
	private String tutor;
	private String cons;
	private String altro;
	private int scad;
	private int grad;
	private int tot;
	private String comp1;
	private String comp2;

	@NotNull (message = "campo obbligatorio")
	private Date data;

	@NotNull (message = "campo obbligatorio")
	private Date anno;
	public Customer getCustomer() {
		return customer;
	}


	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	public String getFascia() {
		return fascia;
	}
	public void setFascia(String fascia) {
		this.fascia = fascia;
	}
	public String getQualifica() {
		return qualifica;
	}
	public void setQualifica(String qualifica) {
		this.qualifica = qualifica;
	}
	public String getCv() {
		return cv;
	}
	public void setCv(String cv) {
		this.cv = cv;
	}
	public String getDocenza() {
		return docenza;
	}
	public void setDocenza(String docenza) {
		this.docenza = docenza;
	}
	public String getCd() {
		return cd;
	}
	public void setCd(String cd) {
		this.cd = cd;
	}
	public String getSw() {
		return sw;
	}
	public void setSw(String sw) {
		this.sw = sw;
	}
	public String getElmat() {
		return elmat;
	}
	public void setElmat(String elmat) {
		this.elmat = elmat;
	}
	public String getRic() {
		return ric;
	}
	public void setRic(String ric) {
		this.ric = ric;
	}
	public String getProg() {
		return prog;
	}
	public void setProg(String prog) {
		this.prog = prog;
	}
	public String getEldid() {
		return eldid;
	}
	public void setEldid(String eldid) {
		this.eldid = eldid;
	}
	public String getTutor() {
		return tutor;
	}
	public void setTutor(String tutor) {
		this.tutor = tutor;
	}
	public String getCons() {
		return cons;
	}
	public void setCons(String cons) {
		this.cons = cons;
	}
	public String getAltro() {
		return altro;
	}
	public void setAltro(String altro) {
		this.altro = altro;
	}
	public int getScad() {
		return scad;
	}
	public void setScad(int scad) {
		this.scad = scad;
	}
	public int getGrad() {
		return grad;
	}
	public void setGrad(int grad) {
		this.grad = grad;
	}
	public int getTot() {
		return tot;
	}
	public void setTot(int tot) {
		this.tot = tot;
	}
	public String getComp1() {
		return comp1;
	}
	public void setComp1(String comp1) {
		this.comp1 = comp1;
	}
	public String getComp2() {
		return comp2;
	}
	public void setComp2(String comp2) {
		this.comp2 = comp2;
	}
	public Date getData() {
		return data;
	}
	public void setData(Date data) {
		this.data = data;
	}
	public Date getAnno() {
		return anno;
	}
	public void setAnno(Date anno) {
		this.anno = anno;
	}

}