package it.primaryteam.webapp.whisper.ledger;

import it.primaryteam.webapp.whisper.core.Repository;

public interface LedgerRepo extends Repository<Long, Ledger>{

	public boolean hasQualification(Long customerId);
}
