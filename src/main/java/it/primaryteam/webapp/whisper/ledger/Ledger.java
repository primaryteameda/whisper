package it.primaryteam.webapp.whisper.ledger;

import java.io.InputStream;
import java.util.Date;
import javax.validation.constraints.NotNull;
import it.primaryteam.webapp.whisper.core.BaseDomain;
import it.primaryteam.webapp.whisper.job.Job;

public class Ledger  extends BaseDomain <Long>{

	private static final long serialVersionUID = 1L; 

	@NotNull (message = "campo obbligatorio")
	private double payment;
	@NotNull (message = "campo obbligatorio")
	private Date date;
	private InputStream receipt;
	private Job job = new Job() ;


	public Job getJob() {
		return job;
	}
	public void setJob(Job job) {
		this.job = job;
	}
	public double getPayment() {
		return payment;
	}
	public void setPayment(double payment) {
		this.payment = payment;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public InputStream getReceipt() {
		return receipt;
	}
	public void setReceipt(InputStream receipt) {
		this.receipt = receipt;
	}
	@Override
	public String toString() {
		return "Ledger [job=" + job + ", payment=" + payment + ", date=" + date + ", receipt=" + receipt + "]";
	}	
}
