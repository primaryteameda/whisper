package it.primaryteam.webapp.whisper.ledger;

import java.io.ByteArrayInputStream;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.enterprise.context.Dependent;
import it.primaryteam.webapp.whisper.core.BaseRepository;
import it.primaryteam.webapp.whisper.core.DataParameter;
import it.primaryteam.webapp.whisper.core.DataParameter.Type;
import it.primaryteam.webapp.whisper.job.Job;
import it.primaryteam.webapp.whisper.ledger.Ledger;

@Dependent
public class LedgerRepository extends BaseRepository <Long, Ledger> {

	public void add(Ledger entity) {

		String sql ="INSERT INTO Ledgers (payment, date, receipt, idjob, visibility) VALUES (?, ?, ?, ?, ?)";


		List<DataParameter> parameters = new ArrayList<>();
		parameters.add(new DataParameter(Type.DOUBLE, entity.getPayment()));
		parameters.add(new DataParameter(Type.TIMESTAMP, new Timestamp(entity.getDate().getTime())));
		parameters.add(new DataParameter(Type.STREAM, entity.getReceipt()));
		parameters.add(new DataParameter(Type.LONG, entity.getJob().getId()));
		parameters.add(new DataParameter(Type.BOOLEAN, true));

		req(sql, parameters);
	}

	@Override
	public Ledger findById(Long id) {

		Job job= new Job();

		Ledger entity= new Ledger();

		String sql = String.format("Select * FROM Ledgers l WHERE id = %d", id);

		List<Map<String, Object>> results = select(sql);

		for(Map<String, Object> result : results) {

			entity.setId((Long)result.get("id"));
			entity.setPayment((Double)result.get("payment"));
			entity.setDate(new Date(((Timestamp)result.get("date")).getTime()));
			entity.setReceipt(new ByteArrayInputStream((byte[])result.get("receipt")));

			job.setId((Long)result.get("idjob"));
			entity.setJob(job);

			entity.setVisibility(true);
		}
		return entity;
	}

	@Override
	public Collection<Ledger> findAllById(Long id) {

		List<Ledger> entitiesList = new ArrayList<Ledger>();


		String sql = String.format("Select * FROM Ledgers l WHERE l.visibility=true AND l.idjob = %d", id);

		List<Map<String, Object>> results = select(sql);

		for(Map<String, Object> result : results) {

			Ledger entity= new Ledger();

			Job job= new Job();
			entity.setId((Long)result.get("id"));
			entity.setPayment((Double)result.get("payment"));
			entity.setDate(new Date(((Timestamp)result.get("date")).getTime()));
			entity.setReceipt(new ByteArrayInputStream((byte[])result.get("receipt")));

			job.setId((Long)result.get("idjob"));
			entity.setJob(job);

			entity.setVisibility(true);

			entitiesList.add(entity);
		}
		return entitiesList;
	}

	@Override
	public void update(Ledger entity) {        

		String sql = String.format(Locale.US, "UPDATE Ledgers l  SET payment = %f, date = '%s', receipt = '%s', idjob = %d, visibility = %b"  
				+ " WHERE l.id = %d", entity.getPayment(),  new Timestamp(entity.getDate().getTime()),entity.getReceipt(), entity.getJob().getId(), entity.isVisibility(), entity.getId());

		req(sql);
	}

	@Override
	public void delete(Ledger entity) {

		String sql = String.format("UPDATE Ledgers l SET l.visibility = false WHERE id = %d", entity.getId());
		System.out.println(sql);
		req(sql);
	}

	@Override
	public Collection<Ledger> findAll() {

		List<Ledger> entitiesList = new ArrayList<Ledger>();

		String sql = "SELECT * FROM Ledgers as l WHERE l.visibility=true";

		List<Map<String, Object>> results = select(sql);

		for(Map<String, Object> result : results) {

			Ledger entity= new Ledger();

			Job job= new Job();

			entity.setId((Long)result.get("id"));
			entity.setPayment((Double)result.get("payment"));
			entity.setDate(new Date(((Timestamp)result.get("date")).getTime()));
			entity.setReceipt(new ByteArrayInputStream((byte[])result.get("receipt")));

			job.setId((Long)result.get("idjob"));
			entity.setJob(job);

			entity.setVisibility(true);

			entitiesList.add(entity);
		}
		return entitiesList;
	}

	public List<Ledger> search(String column, String value) {

		List<Ledger> entitiesList = new ArrayList<Ledger>();

		String sql = "SELECT * FROM Ledgers as l WHERE l.visibility=true AND " + column + " = '" + value + "'";

		List<Map<String, Object>> results = select(sql);

		for(Map<String, Object> result : results) {

			Ledger entity= new Ledger();
			Job job= new Job();

			entity.setId((Long)result.get("id"));
			entity.setPayment((Double)result.get("payment"));
			entity.setDate(new Date(((Timestamp)result.get("date")).getTime()));
			entity.setReceipt(new ByteArrayInputStream((byte[])result.get("receipt")));

			job.setId((Long)result.get("idjob"));
			entity.setJob(job);

			entity.setVisibility(true);

			entitiesList.add(entity);
		}
		return entitiesList;
	}

	public void addLedger(Ledger entity, Long ID) {

		String sql =String.format(Locale.US,"INSERT INTO Ledgers (payment, date, receipt, idjob, visibility) VALUES "
				+ "('%f','%s','%s',%d, %b)", 
				entity.getPayment(),  new Timestamp(entity.getDate().getTime()), entity.getReceipt(), ID, entity.isVisibility());
		req(sql);
	}

}