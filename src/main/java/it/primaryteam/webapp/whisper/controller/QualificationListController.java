package it.primaryteam.webapp.whisper.controller;

import java.util.Collection;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import it.primaryteam.webapp.whisper.core.Repository;
import it.primaryteam.webapp.whisper.customer.Customer;
import it.primaryteam.webapp.whisper.qualification.Qualification;

@ManagedBean		
@ViewScoped  
public class QualificationListController extends BaseController{

	private static final long serialVersionUID = 1L;

	private Qualification qualification;
	private Collection<Qualification> qualifications;
	private Customer customer;
	private Long id= 0L;

	@Inject  
	private Repository<Long, Qualification> qualificationRepository;

	@Inject
	private Repository<Long, Customer> customerRepository;

	public void init() {
		qualification= new Qualification();
		this.qualifications = qualificationRepository.findAll();
		customer= new Customer();
		id= this.getParameters().containsKey("ID") ? Long.parseLong(getParameters().get("ID")) : 0L;
		customer= customerRepository.findById(id);
	}

	public Qualification getQualification() {
		return qualification;
	}

	public void setQualification(Qualification qualification) {
		this.qualification = qualification;
	}

	public Collection<Qualification> getQualifications() {
		return qualifications;
	}

	public void setQualifications(Collection<Qualification> qualifications) {
		this.qualifications = qualifications;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}