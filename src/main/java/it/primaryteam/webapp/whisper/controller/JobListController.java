package it.primaryteam.webapp.whisper.controller;

import java.io.InputStream;
import java.util.Collection;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import org.primefaces.model.DefaultStreamedContent;
import it.primaryteam.webapp.whisper.core.Repository;
import it.primaryteam.webapp.whisper.job.Job;

@ManagedBean		
@ViewScoped  
public class JobListController extends BaseController{

	private static final long serialVersionUID = 1L;

	private Job job;
	private Collection<Job> jobs;
	private Long id;

	@Inject  
	private Repository<Long, Job> jobRepository;

	public void init() {
		job= new Job();
		this.jobs = jobRepository.findAll();

	}

	public Collection<Job> setJobs() {
		return jobRepository.findAll();
	}

	public Collection<Job> getJobs() {
		return jobRepository.findAll();
	}

	public void updateJob() {
		jobRepository.update(job);
	}

	public void setJob(Job job) {
		this.job = job;
	}

	public Job getJob() {
		return job;
	}

	public DefaultStreamedContent getFile(InputStream inputStream) {
		return new DefaultStreamedContent(inputStream, "application/pdf", "contratto.pdf");
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setJobs(Collection<Job> jobs) {
		this.jobs = jobs;
	}
}