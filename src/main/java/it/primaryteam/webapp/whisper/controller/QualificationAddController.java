package it.primaryteam.webapp.whisper.controller;

import java.util.Collection;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import it.primaryteam.webapp.whisper.core.Repository;
import it.primaryteam.webapp.whisper.customer.Customer;
import it.primaryteam.webapp.whisper.qualification.Qualification;

@ManagedBean
@ViewScoped  
public class QualificationAddController extends BaseController{

	private static final long serialVersionUID = 1L;

	private Customer customer;
	private Qualification qualification;
	private Collection<Customer> customers;
	private Long id= 0L;

	@Inject
	private Repository<Long, Qualification> qualificationRepository;

	@Inject
	private Repository <Long, Customer> customerRepository;

	public void init() {
		qualification= new Qualification();
		if (this.getParameters().containsKey("ID")) {
			customer = new Customer();
			customer.setId(Long.parseLong(getParameters().get("ID")));
			qualification.setCustomer(customer);
		}
	}	

	public void addQualification() {	
		qualificationRepository.add(qualification);
		redirect("viewSupplier.xhtml?ID=" + customer.getId());
	}

	public Qualification getQualification() {
		return qualification;
	}

	public void setQualification(Qualification qualification) {
		this.qualification = qualification;
	}

	public Collection<Customer> getCustomers() {
		return customers;
	}

	public void setCustomers(Collection<Customer> customers) {
		this.customers = customers;
	}
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
}