package it.primaryteam.webapp.whisper.controller;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import it.primaryteam.webapp.whisper.core.Repository;
import it.primaryteam.webapp.whisper.feedback.Feedback;

@ManagedBean
@ViewScoped
public class FeedbackUpdateController extends BaseController {

	private static final long serialVersionUID = 1L;

	private Feedback feedback;
	private Long id= 0L;

	@Inject
	private Repository<Long, Feedback> feedbackRepository;

	public void init() {
		this.feedback = new Feedback();
		id= this.getParameters().containsKey("ID") ? Long.parseLong(getParameters().get("ID")) : 0L;
		this.feedback = feedbackRepository.findById(id);
	}

	public void updateFeedback() {
		feedbackRepository.update(feedback);
		redirect("viewFeedback.xhtml?ID=" + feedback.getJob().getId());
	}

	public void setFeedback(Feedback feedback) {
		this.feedback = feedback;
	}

	public Feedback getFeedback() {
		return feedback;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}	
}