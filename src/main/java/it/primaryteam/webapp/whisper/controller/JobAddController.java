package it.primaryteam.webapp.whisper.controller;

import java.io.IOException;
import java.util.Collection;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import org.primefaces.model.UploadedFile;
import it.primaryteam.webapp.whisper.core.Repository;
import it.primaryteam.webapp.whisper.customer.Customer;
import it.primaryteam.webapp.whisper.customer.CustomerRepository;
import it.primaryteam.webapp.whisper.job.Job;

@ManagedBean
@ViewScoped  
public class JobAddController extends BaseController{

	private static final long serialVersionUID = 1L;

	private Job job;
	private Collection<Customer> customers;

	@Inject
	private Repository<Long, Job> jobRepository;

	@Inject
	private CustomerRepository customerRepository;

	private UploadedFile file;

	public void init() {
		job= new Job();
		customers= customerRepository.findAllCandF();
	}

	public void addJob() {
		try {
			job.setContract(file.getInputstream());

		} catch (IOException e) {
			e.printStackTrace();
		}

		jobRepository.add(job);
		redirect("listJob.xhtml");
	}

	public void setJob(Job job) {
		this.job = job;
	}

	public Job getJob() {
		return job;
	}

	public Collection<Customer> getCustomers() {
		return customers;
	}

	public void setCustomers(Collection<Customer> customers) {
		this.customers = customers;
	}

	public UploadedFile getFile() {
		return file;
	}

	public void setFile(UploadedFile file) {
		this.file = file;
	}
}