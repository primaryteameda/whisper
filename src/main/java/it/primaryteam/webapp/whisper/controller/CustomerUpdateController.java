package it.primaryteam.webapp.whisper.controller;

import java.io.IOException;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import org.primefaces.model.UploadedFile;
import it.primaryteam.webapp.whisper.core.Repository;
import it.primaryteam.webapp.whisper.customer.Customer;

@ManagedBean
@ViewScoped
public class CustomerUpdateController extends BaseController {

	private static final long serialVersionUID = 1L;

	private Customer customer;
	private Long id= 0L;
	private UploadedFile file;

	@Inject
	private Repository<Long, Customer> customerRepository;

	public void init() {
		customer = new Customer();
		id= this.getParameters().containsKey("ID") ? Long.parseLong(getParameters().get("ID")) : 0L;
		customer= customerRepository.findById(id);
	}

	public void updateCustomer() {

		try {
			customer.setDocument(file.getInputstream());
			customer.setCv(file.getInputstream());
			customer.setContract(file.getInputstream());
			customer.setProfile(file.getInputstream());

		} catch (IOException e) {
			e.printStackTrace();
		}
		if(customer.getTypology().equals("Cliente")) {
			customerRepository.update(customer);
			redirect("listCustomer.xhtml");
		}
		else if(customer.getTypology().equals("Fornitore")) {
			customerRepository.update(customer);
			redirect("listSupplier.xhtml");
		}
		else{
			customerRepository.update(customer);
			redirect("listPotential.xhtml");
		}

	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Customer getCustomer() {
		return customer;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public UploadedFile getFile() {
		return file;
	}

	public void setFile(UploadedFile file) {
		this.file = file;
	}
}