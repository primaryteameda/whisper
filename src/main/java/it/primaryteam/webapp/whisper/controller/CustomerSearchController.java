package it.primaryteam.webapp.whisper.controller;

import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import it.primaryteam.webapp.whisper.core.Repository;
import it.primaryteam.webapp.whisper.customer.Customer;

@ManagedBean		
@ViewScoped
public class CustomerSearchController extends BaseController{

	private static final long serialVersionUID = 1L;

	private List<Customer> customers;

	@Inject  
	private Repository<Long, Customer> customerRepository;

	private String value= "";
	private String column="";


	public void init() {
		customers=new ArrayList<Customer>();
	}

	public void searchCustomer() {
		this.customers = customerRepository.search(column, value);
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getColumn() {
		return column;
	}

	public void setColumn(String column) {
		this.column = column;
	}

	public List<Customer> getCustomers() {
		return customers;
	}

	public void setCustomers(List<Customer> customers) {
		this.customers = customers;
	}
	
}