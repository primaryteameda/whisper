package it.primaryteam.webapp.whisper.controller;

import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import it.primaryteam.webapp.whisper.core.Repository;
import it.primaryteam.webapp.whisper.job.Job;

@ManagedBean		
@ViewScoped
public class JobSearchController extends BaseController{

	private static final long serialVersionUID = 1L;

	private List<Job> jobs;

	@Inject  
	private Repository<Long, Job> jobRepository;

	private String value;
	private String column;

	public void init() {
		this.jobs = jobRepository.search(column, value);
	}


	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getColumn() {
		return column;
	}

	public void setColumn(String column) {
		this.column = column;
	}

	public List<Job> getJobs() {
		return jobs;
	}

	public void setJobs(List<Job> jobs) {
		this.jobs = jobs;
	}

}