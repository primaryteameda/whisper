package it.primaryteam.webapp.whisper.controller;

import java.io.IOException;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import org.primefaces.model.UploadedFile;
import it.primaryteam.webapp.whisper.core.Repository;
import it.primaryteam.webapp.whisper.customer.Customer;

@ManagedBean
@ViewScoped
public class CustomerAddController extends BaseController {

	private static final long serialVersionUID = 1L;

	private Customer customer;

	private UploadedFile documentFile;

	private UploadedFile cvFile;

	private UploadedFile contractFile;

	private UploadedFile profileFile;

	@Inject
	private Repository<Long, Customer> customerRepository;

	public void init() {
		this.customer = new Customer();
	}

	public void addCustomer() {
		try {
			customer.setDocument(documentFile.getInputstream());
			customer.setCv(cvFile.getInputstream());
			customer.setContract(contractFile.getInputstream());
			customer.setProfile(profileFile.getInputstream());

		} catch (IOException e) {
			e.printStackTrace();
		}


		if(customer.getTypology().equals("Cliente")) {
			customerRepository.add(customer);
			redirect("listCustomer.xhtml");
		}
		else if(customer.getTypology().equals("Fornitore")) {
			customerRepository.add(customer);
			redirect("listSupplier.xhtml");
		}
		else{
			customerRepository.add(customer);
			redirect("listPotential.xhtml");
		}
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Customer getCustomer() {
		return customer;
	}

	public UploadedFile getDocumentFile() {
		return documentFile;
	}

	public void setDocumentFile(UploadedFile documentFile) {
		this.documentFile = documentFile;
	}

	public UploadedFile getCvFile() {
		return cvFile;
	}

	public void setCvFile(UploadedFile cvFile) {
		this.cvFile = cvFile;
	}

	public UploadedFile getContractFile() {
		return contractFile;
	}

	public void setContractFile(UploadedFile contractFile) {
		this.contractFile = contractFile;
	}

	public UploadedFile getProfileFile() {
		return profileFile;
	}

	public void setProfileFile(UploadedFile profileFile) {
		this.profileFile = profileFile;
	}	

}