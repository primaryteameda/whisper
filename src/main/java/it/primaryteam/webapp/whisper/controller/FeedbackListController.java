package it.primaryteam.webapp.whisper.controller;

import java.util.Collection;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import it.primaryteam.webapp.whisper.core.Repository;
import it.primaryteam.webapp.whisper.feedback.Feedback;
import it.primaryteam.webapp.whisper.job.Job;

@ManagedBean		
@ViewScoped  
public class FeedbackListController extends BaseController{

	private static final long serialVersionUID = 1L;

	private Feedback feedback;
	private Collection<Feedback> feedbacks;
	private Job job;
	private Long id= 0L;

	@Inject  
	private Repository<Long, Feedback> feedbackRepository;

	public void init() {
		feedback= new Feedback();
		this.feedbacks = feedbackRepository.findAll();
		job= new Job();
		id= this.getParameters().containsKey("ID") ? Long.parseLong(getParameters().get("ID")) : 0L;

	}

	public Feedback getFeedback() {
		return feedback;
	}

	public void setFeedback(Feedback feedback) {
		this.feedback = feedback;
	}

	public Collection<Feedback> getFeedbacks() {
		return feedbacks;
	}

	public void setFeedbacks(Collection<Feedback> feedbacks) {
		this.feedbacks = feedbacks;
	}

	public Job getJob() {
		return job;
	}

	public void setJob(Job job) {
		this.job = job;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}