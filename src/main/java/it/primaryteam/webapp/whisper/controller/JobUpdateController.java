package it.primaryteam.webapp.whisper.controller;

import java.io.IOException;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import org.primefaces.model.UploadedFile;
import it.primaryteam.webapp.whisper.core.Repository;
import it.primaryteam.webapp.whisper.job.Job;

@ManagedBean
@ViewScoped
public class JobUpdateController extends BaseController {
	private static final long serialVersionUID = 1L;
	private Job job;
	private Long id= 0L;

	@Inject
	private Repository<Long, Job> jobRepository;

	private UploadedFile file;

	public void init() {
		this.job = new Job();
		id= this.getParameters().containsKey("ID") ? Long.parseLong(getParameters().get("ID")) : 0L;
		this.job = jobRepository.findById(id);
	}

	public void updateJob() {
		try {
			job.setContract(file.getInputstream());

		} catch (IOException e) {
			e.printStackTrace();
		}

		jobRepository.update(job);
		redirect("listJob.xhtml");
	}

	public void setJob(Job job) {
		this.job = job;
	}

	public Job getJob() {
		return job;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public UploadedFile getFile() {
		return file;
	}

	public void setFile(UploadedFile file) {
		this.file = file;
	}
}