package it.primaryteam.webapp.whisper.controller;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import it.primaryteam.webapp.whisper.core.Repository;
import it.primaryteam.webapp.whisper.feedback.Feedback;
import it.primaryteam.webapp.whisper.job.Job;
import it.primaryteam.webapp.whisper.job.JobRepo;

@ManagedBean
@ViewScoped
public class FeedbackViewController extends BaseController {

	private static final long serialVersionUID = 1L;

	private Job job;
	private Feedback feedback;
	private Long id= 0L;

	private boolean hasQualification;

	@Inject
	private JobRepo jobRepository;

	@Inject
	private Repository<Long, Feedback> feedbackRepository;

	public void init() {
		job = new Job();
		feedback = new Feedback();
		id= this.getParameters().containsKey("ID") ? Long.parseLong(getParameters().get("ID")) : 0L;
		feedback = feedbackRepository.findById(id);
		job = jobRepository.findById(id);

		if (job != null) {
			hasQualification = jobRepository.hasQualification(job.getId());
		}
	}

	public Job getJob() {
		return job;
	}

	public void setJob(Job job) {
		this.job = job;
	}

	public Feedback getFeedback() {
		return feedback;
	}

	public void setFeedback(Feedback feedback) {
		this.feedback = feedback;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public boolean isHasQualification() {
		return hasQualification;
	}

	public void setHasQualification(boolean hasQualification) {
		this.hasQualification = hasQualification;
	}
}