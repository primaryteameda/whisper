package it.primaryteam.webapp.whisper.controller;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import it.primaryteam.webapp.whisper.core.Repository;
import it.primaryteam.webapp.whisper.customer.Customer;
import it.primaryteam.webapp.whisper.qualification.Qualification;

@ManagedBean
@ViewScoped
public class QualificationUpdateController extends BaseController {

	private static final long serialVersionUID = 1L;

	private Customer customer;
	private Qualification qualification;
	private Long id= 0L;

	@Inject
	private Repository<Long, Qualification> qualificationRepository;

	@Inject
	private Repository<Long, Customer> customerRepository;

	public void init() {
		customer = new Customer();
		this.qualification = new Qualification();
		id= this.getParameters().containsKey("ID") ? Long.parseLong(getParameters().get("ID")) : 0L;
		this.qualification = qualificationRepository.findById(id);
		customer= customerRepository.findById(id);
	}

	public void updateQualification() {
		qualificationRepository.update(qualification);
		redirect("viewSupplier.xhtml?ID=" + customer.getId());
	}

	public Qualification getQualification() {
		return qualification;
	}

	public void setQualification(Qualification qualification) {
		this.qualification = qualification;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
}