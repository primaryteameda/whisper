package it.primaryteam.webapp.whisper.controller;

import java.io.IOException;
import java.util.Collection;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import org.primefaces.model.UploadedFile;
import it.primaryteam.webapp.whisper.core.Repository;
import it.primaryteam.webapp.whisper.job.Job;
import it.primaryteam.webapp.whisper.ledger.Ledger;

@ManagedBean		
@ViewScoped
public class LedgerAddController  extends BaseController {

	private static final long serialVersionUID = 1L;

	private Job job;
	private Ledger ledger;
	private Collection<Job> jobs;
	private Long id= 0L;

	@Inject  
	private Repository<Long, Ledger> ledgerRepository;

	private UploadedFile file;

	public void init() {
		ledger= new Ledger();
		job = new Job();

		if (this.getParameters().containsKey("ID")) {
			job.setId(Long.parseLong(getParameters().get("ID")));

			ledger.setJob(job);
		}
	}

	public void addLedger() {
		try {
			ledger.setReceipt(file.getInputstream());

		} catch (IOException e) {
			e.printStackTrace();
		}
		ledgerRepository.add(ledger);
		redirect("listLedger.xhtml?ID=" + job.getId());
	}

	public Ledger getLedger() {
		return ledger;
	}

	public void setLedger(Ledger ledger) {
		this.ledger = ledger;
	}

	public Collection<Job> getJobs() {
		return jobs;
	}

	public void setJobs(Collection<Job> jobs) {
		this.jobs = jobs;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Job getJob() {
		return job;
	}

	public void setJob(Job job) {
		this.job = job;
	}

	public UploadedFile getFile() {
		return file;
	}

	public void setFile(UploadedFile file) {
		this.file = file;
	}

}