package it.primaryteam.webapp.whisper.controller;

import java.io.InputStream;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import org.primefaces.model.DefaultStreamedContent;
import it.primaryteam.webapp.whisper.customer.Customer;
import it.primaryteam.webapp.whisper.customer.CustomerRepo;

@ManagedBean
@ViewScoped
public class CustomerViewController extends BaseController {

	private static final long serialVersionUID = 1L;

	private Customer customer;
	private Long id= 0L;

	private boolean hasQualification;

	private boolean hasDownload;

	@Inject
	private CustomerRepo customerRepository;

	public void init() {
		customer = new Customer();
		id= this.getParameters().containsKey("ID") ? Long.parseLong(getParameters().get("ID")) : 0L;
		customer= customerRepository.findById(id);

		if (customer != null) {
			hasQualification = customerRepository.hasQualification(customer.getId());
		}

	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Customer getCustomer() {
		return customer;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public boolean isHasQualification() {
		return hasQualification;
	}

	public void setHasQualification(boolean hasQualification) {
		this.hasQualification = hasQualification;
	}

	public boolean isHasDownload() {
		return hasDownload;
	}

	public void setHasDownload(boolean hasDownload) {
		this.hasDownload = hasDownload;
	}

	public DefaultStreamedContent getDocument(InputStream inputStream) {
		return new DefaultStreamedContent(inputStream, "application/pdf", "documento.pdf");
	}
	public DefaultStreamedContent getCv(InputStream inputStream) {
		return new DefaultStreamedContent(inputStream, "application/pdf", "cv.pdf");
	}
	public DefaultStreamedContent getContract(InputStream inputStream) {
		return new DefaultStreamedContent(inputStream, "application/pdf", "contratto.pdf");
	}
	public DefaultStreamedContent getProfile(InputStream inputStream) {
		return new DefaultStreamedContent(inputStream, "application/pdf", "profilo.pdf");
	}
}