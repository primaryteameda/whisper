package it.primaryteam.webapp.whisper.controller;

import java.util.Collection;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import it.primaryteam.webapp.whisper.core.Repository;
import it.primaryteam.webapp.whisper.feedback.Feedback;
import it.primaryteam.webapp.whisper.job.Job;

@ManagedBean
@ViewScoped
public class FeedbackAddController extends BaseController {

	private static final long serialVersionUID = 1L;

	private Feedback feedback;
	private Collection<Job> jobs;
	private Long id= 0L;

	@Inject  
	private Repository<Long, Feedback> feedbackRepository; 

	public void init() {
		Job job = new Job();
		feedback= new Feedback();
		if (this.getParameters().containsKey("ID")) {

			job.setId(Long.parseLong(getParameters().get("ID")));
			feedback.setJob(job);
		}
	}

	public void addFeedback() {
		feedbackRepository.add(feedback);
		redirect("viewFeedback.xhtml?ID=" + feedback.getJob().getId());
	}

	public Feedback getFeedback() {
		return feedback;
	}

	public void setFeedback(Feedback feedback) {
		this.feedback = feedback;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Collection<Job> getJobs() {
		return jobs;
	}

	public void setJobs(Collection<Job> jobs) {
		this.jobs = jobs;
	}
}