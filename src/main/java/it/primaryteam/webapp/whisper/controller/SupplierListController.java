package it.primaryteam.webapp.whisper.controller;

import java.util.Collection;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import it.primaryteam.webapp.whisper.core.Repository;
import it.primaryteam.webapp.whisper.customer.Customer;

@ManagedBean	
@ViewScoped
public class SupplierListController extends BaseController {

	private static final long serialVersionUID = 1L;

	private Customer customer;
	private Collection<Customer> customers;
	String value ="";
	
	private Customer candidateToDelete;

	@Inject  
	private Repository <Long, Customer> customerRepository;

	public void init() {
		customers = customerRepository.search("typology", "Fornitore");
	}

	public void deleteCustomer() {
		customerRepository.delete(candidateToDelete);
		redirect("listSupplier.xhtml");
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Customer getCustomer() {
		return customer;
	}

	public Collection<Customer> getCustomers() {
		return customers;
	}

	public void setCustomers(Collection<Customer> customers) {
		this.customers = customers;
	}

	public void setCandidateToDelete(Customer customer) {
		this.candidateToDelete = customer;
	}
}
