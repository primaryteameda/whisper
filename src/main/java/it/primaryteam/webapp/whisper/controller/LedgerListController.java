package it.primaryteam.webapp.whisper.controller;

import java.io.InputStream;
import java.util.Collection;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import org.primefaces.model.DefaultStreamedContent;
import it.primaryteam.webapp.whisper.core.Repository;
import it.primaryteam.webapp.whisper.job.Job;
import it.primaryteam.webapp.whisper.ledger.Ledger;

@ManagedBean		
@ViewScoped  
public class LedgerListController extends BaseController{

	private static final long serialVersionUID = 1L;

	private Job job;
	private Ledger ledger;
	private Collection<Job> jobs;
	private Long id= 0L;

	private Collection<Ledger> ledgers;

	@Inject  
	private Repository<Long, Job> jobRepository;

	@Inject 
	private Repository<Long, Ledger> ledgerRepository;

	private Ledger candidateToDelete;

	public void init() {
		ledger = new Ledger();
		job = new Job();
		id= this.getParameters().containsKey("ID") ? Long.parseLong(getParameters().get("ID")) : 0L;
		ledgers = ledgerRepository.findAllById(id);
		job= jobRepository.findById(id);
	}

	public void deleteLedger() {
		ledgerRepository.delete(candidateToDelete);
		redirect("listLedger.xhtml?ID=" + job.getId());
	}

	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}

	public Collection<Ledger> getLedgers() {
		return ledgers;
	}

	public void setLedgers(Collection<Ledger> ledgers) {
		this.ledgers = ledgers;
	}

	public Collection<Job> getJobs() {
		return jobs;
	}

	public void setJobs(Collection<Job> jobs) {
		this.jobs = jobs;
	}

	public Job getJob() {
		return job;
	}

	public void setJob(Job job) {
		this.job = job;
	}

	public void setCandidateToDelete(Ledger ledger) {
		this.candidateToDelete = ledger;
	}

	public Ledger getLedger() {
		return ledger;
	}

	public void setLedger(Ledger ledger) {
		this.ledger = ledger;
	}

	public DefaultStreamedContent getFile(InputStream inputStream) {
		return new DefaultStreamedContent(inputStream, "application/pdf", "ricevuta.pdf");
	}
}