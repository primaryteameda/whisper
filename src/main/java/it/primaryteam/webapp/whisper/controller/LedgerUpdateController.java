package it.primaryteam.webapp.whisper.controller;

import java.io.IOException;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import org.primefaces.model.UploadedFile;
import it.primaryteam.webapp.whisper.core.Repository;
import it.primaryteam.webapp.whisper.job.Job;
import it.primaryteam.webapp.whisper.ledger.Ledger;

@ManagedBean
@ViewScoped
public class LedgerUpdateController extends BaseController {

	private static final long serialVersionUID = 1L;

	private Ledger ledger;
	private Job job;
	private Long idLedger= 0L;
	private Long idJob = 0L;

	private UploadedFile file;

	@Inject
	private Repository<Long, Ledger> ledgerRepository;

	@Inject
	private Repository<Long, Job> jobRepository;

	public void init() {
		this.ledger = new Ledger();
		idLedger= this.getParameters().containsKey("ID") ? Long.parseLong(getParameters().get("ID")) : 0L;
		this.ledger = ledgerRepository.findById(idLedger);
		idJob= this.ledger.getJob().getId();
	}

	public void updateLedger() {
		try {
			ledger.setReceipt(file.getInputstream());

		} catch (IOException e) {
			e.printStackTrace();
		}

		ledgerRepository.update(ledger);
		redirect("listLedger.xhtml?ID=" + idJob);
	}

	public Ledger getLedger() {
		return ledger;
	}

	public void setLedger(Ledger ledger) {
		this.ledger = ledger;
	}

	public Job getJob() {
		return job;
	}

	public void setJob(Job job) {
		this.job = job;
	}

	public Long getIdLedger() {
		return idLedger;
	}

	public void setIdLedger(Long idLedger) {
		this.idLedger = idLedger;
	}

	public Long getIdJob() {
		return idJob;
	}

	public void setIdJob(Long idJob) {
		this.idJob = idJob;
	}

	public UploadedFile getFile() {
		return file;
	}

	public void setFile(UploadedFile file) {
		this.file = file;
	}

}