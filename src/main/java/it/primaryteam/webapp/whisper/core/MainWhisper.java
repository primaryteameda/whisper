package it.primaryteam.webapp.whisper.core;

import java.util.Date;
import it.primaryteam.webapp.whisper.customer.Customer;

import it.primaryteam.webapp.whisper.feedback.Feedback;

import it.primaryteam.webapp.whisper.job.Job;
import it.primaryteam.webapp.whisper.job.JobRepository;
import it.primaryteam.webapp.whisper.login.Login;
import it.primaryteam.webapp.whisper.login.LoginRepository;
import it.primaryteam.webapp.whisper.qualification.Qualification;
import it.primaryteam.webapp.whisper.qualification.QualificationRepository;

public class MainWhisper {

	public static void main(String[] args) {

		Customer entity = new Customer();
		Login login = new Login();
		Job job = new Job();
		Qualification quali = new Qualification();
		Feedback feed = new Feedback();

		//CustomerRepository asd = new CustomerRepository();
		LoginRepository log = new LoginRepository();
		JobRepository jb = new JobRepository();
		QualificationRepository qua = new QualificationRepository();

		//FeedbackRepository fe= new FeedbackRepository();

		entity.setId(1L);
		entity.setCompany("company");
		entity.setOwner("owner");
		entity.setAddress("address");
		entity.setCity("city");
		entity.setIva("iva");
		entity.setCf("cf");
		entity.setTel("tel");
		entity.setEmail("email");
		entity.setTypology("typology");
		//entity.setAtDocument("document");
		//entity.setAtCv("cv");
		//entity.setAtContract("contract");
		//entity.setAtProfile("profile");
		entity.setVisibility(true);

		login.setId(1L);
		login.setUsername("a@a");
		login.setPassword("a");
		login.setLevel("1");
		login.setVisibility(true);

		Date start = new Date();
		Date expiry = new Date();

		job.setId(1L);		
		start.setDate(02/02/2018);
		job.setStart(start);		
		expiry.setDate(02/02/2018);
		job.setExpiry(expiry);

		job.setCode("code");
		job.setDescription("description");
		job.setTot(0.0);
		// job.setAtcontract("contract");
		job.setCustomer(entity);
		job.setVisibility(true);

		Date data = new Date();
		Date anno = new Date();	

		quali.setId(54L);
		quali.setFascia("fascia");
		quali.setQualifica("qualifica");
		quali.setCv("si");
		quali.setDocenza("si");
		quali.setCd("si");
		quali.setSw("si");
		quali.setElmat("si");	
		quali.setRic("si");
		quali.setProg("si");
		quali.setEldid("si");
		quali.setTutor("si");	
		quali.setCons("si");	
		quali.setAltro("altro");
		quali.setScad(0);
		quali.setGrad(0);
		quali.setTot(0);
		quali.setComp1("comp1");
		quali.setComp2("comp2");
		data.setDate(02/12/2018);
		anno.setDate(12/12/2018);
		quali.setCustomer(entity);
		quali.setVisibility(true);		

		feed.setId(2L);
		feed.setJob(job);
		feed.setQ1("q1");
		feed.setQ2("q2");
		feed.setQ2bis("q2bis");

		feed.setQ3("q3");
		feed.setQ4("q4");
		feed.setQ4bis("q4bis");
		feed.setQ5("q5");
		feed.setQ6("q6");
		feed.setQ7("q7");

		//asd.add(entity);
		//asd.update(entity);
		//asd.delete(entity);
		//asd.search(entity.getCompany(), "company");
		//System.out.println(asd.search(entity.getCompany(), "company"));
		//System.out.println(asd.findById(7L));
		//System.out.println(asd.findAll());
		//log.getLogin(login);
		//System.out.println(log.getLogin().toString());
		//jb.add(job);
		//qua.add(quali);
		//fe.add(feed);
		//qua.update(quali);
		//fe.findAll();
		//System.out.println(fe.findAll());
	}

}
