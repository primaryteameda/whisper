package it.primaryteam.webapp.whisper.core;

import java.io.Serializable;

public abstract class BaseDomain<ID extends Serializable> implements Serializable {

	private static final long serialVersionUID = 1L;

	private ID id;
	private boolean visibility;

	public boolean isVisibility() {
		return visibility;
	}

	public void setVisibility(boolean visibility) {
		this.visibility = visibility;
	}

	public ID getId() {
		return id;
	}

	public void setId(ID id) {
		this.id = id;
	}

}