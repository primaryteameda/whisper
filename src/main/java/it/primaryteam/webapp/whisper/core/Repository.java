package it.primaryteam.webapp.whisper.core;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

public interface Repository<ID extends Serializable, D extends BaseDomain<ID>> {
	
	public void add(D entity); 
	
	public D findById(ID id);
	
	public void update (D entity);

	public void delete (D entity);
	
	public Collection<D> findAll();
	
	public List<D> search(String column, String value);

	Collection<D> findAllById(Long id);

}