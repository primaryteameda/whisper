package it.primaryteam.webapp.whisper.core;

import java.io.InputStream;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class BaseRepository<ID extends Serializable,  D extends BaseDomain<ID>> implements Repository<ID, D>{

	//private static final String URL="jdbc:mysql://localhost:3306/whisper?serverTimezone=UTC&autoReconnect=true&useSSL=false";

	private static final String URL="jdbc:mysql://192.168.30.111/whisper?serverTimezone=UTC&autoReconnect=true&useSSL=false";

	private static final String USER = "root";
	private static final String PASSWORD = "test";

	public void req(String sql) {
		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD))  {
			Statement statement = connection.createStatement();
			statement.executeUpdate(sql);

		} catch (SQLException e) {
			System.out.println(" DataBase Open Error ");
			e.printStackTrace();
		}
	}

	public void req(String sql, List<DataParameter> parameters) {
		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD);
				PreparedStatement statement = connection.prepareStatement(sql))  {
			for (int i = 0; i < parameters.size(); i++) {
				DataParameter parameter = parameters.get(i);

				switch (parameter.getType()) {

				case INT:
					statement.setInt(i + 1, (Integer)parameter.getValue());
					break;

				case LONG:
					statement.setLong(i + 1, (Long)parameter.getValue());
					break;

				case FLOAT:
					statement.setFloat(i + 1, (Float)parameter.getValue());
					break;

				case DOUBLE:
					statement.setDouble(i + 1, (Double)parameter.getValue());
					break;

				case BOOLEAN:
					statement.setBoolean(i + 1, (Boolean)parameter.getValue());
					break;

				case STRING:
					statement.setString(i + 1, (String)parameter.getValue());
					break;

				case TIMESTAMP:
					statement.setTimestamp(i + 1, (Timestamp)parameter.getValue());
					break;

				case STREAM:
					statement.setBinaryStream(i + 1, (InputStream) parameter.getValue());
					break;

				default:
					break;
				}
			}

			statement.executeUpdate();

		} catch (SQLException e) {
			System.out.println(" DataBase Open Error ");
			e.printStackTrace();
		}
	}

	public List<Map<String, Object>> select(String sql) {
		List<Map<String, Object>> results = new ArrayList<Map<String, Object>>();

		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery(sql);

			ResultSetMetaData rsmd = resultSet.getMetaData();
			int columnCount = rsmd.getColumnCount();

			while(resultSet.next()) {
				Map<String, Object> result = new HashMap<>();

				for (int i = 1; i <= columnCount; i++) {
					String columnName = rsmd.getColumnName(i);
					result.put(columnName, resultSet.getObject(columnName));
				}
				results.add(result);
			}
		} catch (SQLException e) {
			System.out.println(" DataBase Open Error ");
			e.printStackTrace();
		}
		return results;
	}
}