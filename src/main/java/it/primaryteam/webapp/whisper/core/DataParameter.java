package it.primaryteam.webapp.whisper.core;

public class DataParameter {

	private Type type;
	private Object value;

	public DataParameter() {}

	public DataParameter(Type type, Object value)  {
		this.type = type;
		this.value = value;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}

	public enum Type {
		INT, LONG, FLOAT, DOUBLE, BOOLEAN, STRING, TIMESTAMP, STREAM;
	}
}