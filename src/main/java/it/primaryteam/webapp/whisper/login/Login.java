package it.primaryteam.webapp.whisper.login;

import it.primaryteam.webapp.whisper.core.BaseDomain;
import it.primaryteam.webapp.whisper.login.validators.Username;

public class Login extends BaseDomain <Long> {

	private static final long serialVersionUID = 1L;

	@Username
	private String username;
	private String password;
	private String level;
	private boolean visibility;

	public Login () {

		this.username = "";
		this.password = "";
		this.level = "";
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}


	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isVisibility() {
		return visibility;
	}

	public void setVisibility(boolean visibility) {
		this.visibility = visibility;
	}

	@Override
	public String toString() 
	{
		return this.username + " " + this.password + " " + this.level;

	}
}