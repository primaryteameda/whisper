package it.primaryteam.webapp.whisper.customer;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import javax.enterprise.context.Dependent;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import it.primaryteam.webapp.whisper.customer.Customer;
import it.primaryteam.webapp.whisper.core.BaseRepository;

@Dependent
public class CustomerRepository extends BaseRepository <Long, Customer> implements CustomerRepo {


	public void add(Customer entity) {


		List<Customer> customers = new ArrayList<Customer>();

		customers= this.search( "cf", entity.getCf());

		if (customers.size() > 0) {
			FacesContext.getCurrentInstance().addMessage("variabile messaggio", new FacesMessage("CF ESISTENTE"));
		}
		else {
			String sql = String.format("INSERT INTO Customers (company, owner, address, city, iva, cf, tel, email, typology, document, cv, "
					+ "contract, profile, visibility) VALUES "
					+ "('%s','%s','%s', '%s','%s','%s','%s','%s','%s','%s','%s','%s', '%s', true)", 
					entity.getCompany(), entity.getOwner(), entity.getAddress(), entity.getCity(), entity.getIva(), entity.getCf(), entity.getTel(), 
					entity.getEmail(), entity.getTypology(), entity.getDocument(), entity.getCv(), entity.getContract(), 
					entity.getProfile(), entity.isVisibility());

			req(sql);

		}

	}

	@Override
	public Customer findById(Long id) {

		String sql = String.format("Select * FROM Customers c WHERE id = %d", id);

		List<Map<String, Object>> results = select(sql);

		Customer entity = new Customer();

		for(Map<String, Object> result : results) {

			entity.setId((Long)result.get("id"));
			entity.setCompany((String)result.get("company"));
			entity.setOwner((String)result.get("owner"));
			entity.setAddress((String)result.get("address"));
			entity.setCity((String)result.get("city"));
			entity.setIva((String)result.get("iva"));
			entity.setCf((String)result.get("cf"));
			entity.setTel((String)result.get("tel"));
			entity.setEmail((String)result.get("email"));
			entity.setTypology((String)result.get("typology"));
			entity.setDocument(new ByteArrayInputStream((byte[])result.get("document")));
			entity.setCv(new ByteArrayInputStream((byte[])result.get("cv")));
			entity.setContract(new ByteArrayInputStream((byte[])result.get("contract")));
			entity.setProfile(new ByteArrayInputStream((byte[])result.get("profile")));
			entity.setVisibility(true);

		}
		return entity;
	}

	@Override
	public void update(Customer entity) {

		String sql = String.format("UPDATE Customers c  SET company = '%s', owner = '%s', address = '%s', city = '%s', iva = '%s', cf = '%s', tel = '%s', email = '%s',"
				+ " typology = '%s' , document = '%s', cv = '%s', contract = '%s', profile = '%s',visibility = %b"  
				+ " WHERE c.id = %d", entity.getCompany(), entity.getOwner(), entity.getAddress(), entity.getCity(), entity.getIva(), entity.getCf(), entity.getTel(), 
				entity.getEmail(), entity.getTypology(), entity.getDocument(), entity.getCv(), entity.getContract(), 
				entity.getProfile(), entity.isVisibility(), entity.getId());

		req(sql);
	}

	@Override
	public void delete(Customer entity) {

		String sql = String.format("UPDATE Customers c SET c.visibility = false WHERE id = %d", entity.getId());

		req(sql);
	}

	@Override
	public Collection<Customer> findAll() {

		String sql = "SELECT * FROM Customers as c WHERE c.visibility=true ";

		List<Customer> entitiesList = readOutput (sql);

		return entitiesList;
	}

	public Collection<Customer> findAllCandF() {

		String sql = "SELECT * FROM Customers as c WHERE c.visibility=true AND c.typology <> 'Contatto' ";

		List<Customer> entitiesList = readOutput (sql);

		return entitiesList;
	}

	public List<Customer> search(String column, String value) {


		String sql = "SELECT * FROM Customers as c WHERE c.visibility = true AND " + column + " = '" + value+"'";
		List<Customer> entitiesList = readOutput (sql);

		return entitiesList;
	}


	private  List<Customer> readOutput(String sql)  {

		List<Customer> entitiesList = new ArrayList<Customer>();


		List<Map<String, Object>> results = select(sql);

		for(Map<String, Object> result : results) {

			Customer entity = new Customer();

			entity.setId((Long)result.get("id"));
			entity.setCompany((String)result.get("company"));
			entity.setOwner((String)result.get("owner"));
			entity.setAddress((String)result.get("address"));
			entity.setCity((String)result.get("city"));
			entity.setIva((String)result.get("iva"));
			entity.setCf((String)result.get("cf"));
			entity.setTel((String)result.get("tel"));
			entity.setEmail((String)result.get("email"));
			entity.setTypology((String)result.get("typology"));
			entity.setDocument(new ByteArrayInputStream((byte[])result.get("document")));
			entity.setCv(new ByteArrayInputStream((byte[])result.get("cv")));
			entity.setContract(new ByteArrayInputStream((byte[])result.get("contract")));
			entity.setProfile(new ByteArrayInputStream((byte[])result.get("profile")));
			entity.setVisibility(true);

			entitiesList.add(entity);
		}
		return entitiesList;
	}

	@Override
	public Collection<Customer> findAllById(Long id) {
		return null;
	}

	@Override
	public boolean hasQualification(Long customerId) {
		String sql = "SELECT * FROM customers c " 
				+ "INNER JOIN qualifications q ON c.id = q.idcustomer " 
				+ "WHERE c.id = " + customerId;			    

		List<Customer> entitiesList = readOutput(sql);

		return entitiesList.size() > 0;
	}
}