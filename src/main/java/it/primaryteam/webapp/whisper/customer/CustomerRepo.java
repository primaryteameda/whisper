package it.primaryteam.webapp.whisper.customer;

import it.primaryteam.webapp.whisper.core.Repository;

public interface CustomerRepo extends Repository<Long, Customer>  {

	public boolean hasQualification(Long customerId);

}
