package it.primaryteam.webapp.whisper.customer;

import java.io.InputStream;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import it.primaryteam.webapp.whisper.core.BaseDomain;

public class Customer extends BaseDomain <Long> {

	private static final long serialVersionUID = 1L;

	public static final String CUSTOMER ="Cliente";
	public static final String SUPPLIER ="Fornitore";
	public static final String POTENTIAL="Contatto";

	@NotBlank (message = "campo obbligatorio")
	private String company; 
	@NotBlank (message = "campo obbligatorio")
	private String owner;
	@NotBlank (message = "campo obbligatorio")
	private String address;
	@NotBlank (message = "campo obbligatorio")
	private String city;
	@NotBlank (message = "campo obbligatorio")
	@Size(min = 11, max = 11, message = "Inserire 11 numeri")
	private String iva;
	@NotBlank (message = "campo obbligatorio")
	private String cf;
	@Pattern(message="inserire solo numeri" , regexp = "[+-]?(([0-9][0-9]*)|(0))([.,][0-9]+)?|(^$)")
	private String tel;
	@Email
	private String email;
	private String typology;  
	private InputStream document;
	private InputStream cv;
	private InputStream contract; 
	private InputStream profile;


	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}

	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getIva() {
		return iva;
	}
	public void setIva(String iva) {
		this.iva = iva;
	}
	public String getCf() {
		return cf;
	}
	public void setCf(String cf) {
		this.cf = cf;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getTypology() {
		return typology;
	}
	public void setTypology(String typology) {
		this.typology = typology;
	}

	public InputStream getDocument() {
		return document;
	}
	public void setDocument(InputStream document) {
		this.document = document;
	}
	public InputStream getCv() {
		return cv;
	}
	public void setCv(InputStream cv) {
		this.cv = cv;
	}
	public InputStream getContract() {
		return contract;
	}
	public void setContract(InputStream contract) {
		this.contract = contract;
	}
	public InputStream getProfile() {
		return profile;
	}
	public void setProfile(InputStream profile) {
		this.profile = profile;
	}
	@Override
	public String toString() {

		return this.company+ " " +  this.owner 
				+ " " + this.address + " " + this.city + " " + this.iva+ " " +   this.cf + " " + this.tel 
				+ " " + this.email + " " +  this.typology + " " + this.document  + " " 
				+ this.cv + " " + this.contract + " " + this.profile ;				
	}

}
