package it.primaryteam.webapp.whisper.job;

import it.primaryteam.webapp.whisper.core.Repository;

public interface JobRepo extends Repository<Long, Job>  {

	public boolean hasQualification(Long jobId);
}