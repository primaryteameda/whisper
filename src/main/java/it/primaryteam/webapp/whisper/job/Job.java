package it.primaryteam.webapp.whisper.job;

import java.io.InputStream;
import java.util.Date;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import it.primaryteam.webapp.whisper.core.BaseDomain;
import it.primaryteam.webapp.whisper.customer.Customer;

public class Job extends BaseDomain <Long> {

	private static final long serialVersionUID = 1L; 

	@NotNull (message = "campo obbligatorio")
	private Date start;

	@NotNull (message = "campo obbligatorio")
	private Date expiry;

	@NotBlank (message = "campo obbligatorio")
	private String code;

	@NotBlank (message = "campo obbligatorio")
	private String description;

	@NotNull (message = "campo obbligatorio")
	private Double tot;
	private InputStream contract;
	private Customer customer = new Customer();

	public Customer getCustomer() {
		return customer;
	}
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Date getStart() {
		return start;
	}
	public void setStart(Date start) {
		this.start = start;
	}
	public Date getExpiry() {
		return expiry;
	}
	public void setExpiry(Date expiry) {
		this.expiry = expiry;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Double getTot() {
		return tot;
	}
	public void setTot(Double tot) {
		this.tot = tot;
	}
	public InputStream getContract() {
		return contract;
	}
	public void setContract(InputStream contract) {
		this.contract = contract;
	}
	@Override
	public String toString() {
		return "Job [start=" + start + ", expiry=" + expiry + ", code=" + code + ", description=" + description
				+ ", tot=" + tot + ", contract=" + contract + ", customer=" + customer + "]";
	}
}
