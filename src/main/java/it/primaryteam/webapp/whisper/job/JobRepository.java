package it.primaryteam.webapp.whisper.job;

import java.io.ByteArrayInputStream;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.enterprise.context.Dependent;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import it.primaryteam.webapp.whisper.core.BaseRepository;
import it.primaryteam.webapp.whisper.core.DataParameter;
import it.primaryteam.webapp.whisper.core.DataParameter.Type;
import it.primaryteam.webapp.whisper.customer.Customer;

@Dependent
public class JobRepository extends BaseRepository <Long, Job> implements JobRepo {

	public void add(Job entity) {

		if( entity.getStart().after(entity.getExpiry()) )
		{

			FacesContext.getCurrentInstance().addMessage("variabile messaggio", new FacesMessage("ERRORE DATA INIZALE MAGGIORE DI DATA FINALE"));
		}

		else {
			String sql = "INSERT INTO Jobs (start, expiry, code, description, tot, contract, idcustomer, visibility) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";

			List<DataParameter> parameters = new ArrayList<>();
			parameters.add(new DataParameter(Type.TIMESTAMP, new Timestamp(entity.getStart().getTime())));
			parameters.add(new DataParameter(Type.TIMESTAMP, new Timestamp(entity.getExpiry().getTime())));
			parameters.add(new DataParameter(Type.STRING, entity.getCode()));
			parameters.add(new DataParameter(Type.STRING, entity.getDescription()));
			parameters.add(new DataParameter(Type.DOUBLE, entity.getTot()));
			parameters.add(new DataParameter(Type.STREAM, entity.getContract()));
			parameters.add(new DataParameter(Type.LONG, entity.getCustomer().getId()));
			parameters.add(new DataParameter(Type.BOOLEAN, true));

			req(sql, parameters);
		}
	}

	@Override
	public Job findById(Long id) {

		Customer customer = new Customer();
		Job entity= new Job();

		String sql = String.format("Select * FROM Jobs j WHERE j.visibility=true AND id = %d", id);

		List<Map<String, Object>> results = select(sql);

		for(Map<String, Object> result : results) {

			entity.setId((Long)result.get("id"));
			entity.setStart(new Date(((Timestamp)result.get("start")).getTime()));
			entity.setExpiry(new Date(((Timestamp)result.get("expiry")).getTime()));
			entity.setCode((String)result.get("code"));
			entity.setDescription((String)result.get("description"));
			entity.setTot((Double)result.get("tot"));
			entity.setContract(new ByteArrayInputStream((byte[])result.get("contract")));

			customer.setId((Long)result.get("idcustomer"));

			entity.setCustomer(customer);
			entity.setVisibility(true);

		}
		return entity;
	}

	@Override
	public void update(Job entity) {

		String sql = String.format(Locale.US,"UPDATE Jobs j  SET start = '%s', expiry = '%s', code = '%s', description = '%s', "
				+ "tot = %f, contract = '%s', idcustomer= %d, visibility = %b"  
				+ " WHERE j.id = %d", new Timestamp(entity.getStart().getTime()), new Timestamp(entity.getExpiry().getTime()), entity.getCode(), entity.getDescription(), 
				entity.getTot(), entity.getContract(), entity.getCustomer().getId(), entity.isVisibility(), entity.getId());
		req(sql);
	}

	@Override
	public void delete(Job entity) {

		String sql = String.format("UPDATE Jobs j SET j.visibility = false WHERE id = %d", entity.getId());

		req(sql);
	}

	@Override
	public Collection<Job> findAll() {

		String sql = "SELECT * FROM Jobs as j WHERE j.visibility=true ";

		List<Job> entitiesList = readOutput (sql);

		return entitiesList;
	}

	public List<Job> search(String column, String value) {


		String sql = "SELECT * FROM Jobs as j WHERE j.visibility=true AND " + column + " like '%" + value + "%'";

		List<Job> entitiesList = readOutput (sql);

		return entitiesList;
	}

	@Override
	public Collection<Job> findAllById(Long id) {
		return null;
	}

	@Override
	public boolean hasQualification(Long jobId) {
		String sql = "SELECT * FROM jobs j " 
				+ "INNER JOIN feedbacks f ON j.id = f.idjob " 
				+ "WHERE j.id = " + jobId;			    

		List<Job> entitiesList = readOutput(sql);

		return entitiesList.size() > 0;
	}

	private  List<Job> readOutput(String sql)  {

		List<Job> entitiesList = new ArrayList<Job>();


		List<Map<String, Object>> results = select(sql);

		for(Map<String, Object> result : results) {

			Job  entity= new Job();
			Customer customer = new Customer();

			entity.setId((Long)result.get("id"));
			entity.setStart(new Date(((Timestamp)result.get("start")).getTime()));
			entity.setExpiry(new Date(((Timestamp)result.get("expiry")).getTime()));
			entity.setCode((String)result.get("code"));
			entity.setDescription((String)result.get("description"));
			entity.setTot((Double)result.get("tot"));
			entity.setContract(new ByteArrayInputStream((byte[])result.get("contract")));

			customer.setId((Long)result.get("idcustomer"));
			entity.setCustomer(customer);

			entity.setVisibility(true);



			entitiesList.add(entity);
		}
		return entitiesList;
	}
}