Il sistema permette ad un'azienda di consulenza di raggruppare informazioni su fornitori e clienti e la loro qualificazione. In particolar modo il sistema rende pi� semplice ed efficace il processo di raccolta, utilizzo ed aggiornamento delle informazioni e dei documenti in essi contenuti. 

Il sistema pu� essere utilizzato da un amministratore, il quale tramite un'area privata (accessibile attraverso login e password), pu� aggiungere dati utilizzabili dall' azienda.
